//
//  Photo.swift
//  FlickrImageGallery
//
//  Created by Apple on 29/03/2017.
//  Copyright © 2017 Tom. All rights reserved.
//

import Foundation
import CoreLocation

struct Photo {
    
    // Ex. obj JSON Flikr photo :
    //"id": "33587061971", "owner": "130674893@N07", "secret": "3bc95b402e", "server": "2817", "farm": 3, "title": "From the air, South Island NZ", "ispublic": 1, "isfriend": 0, "isfamily": 0 ,
    
    var farm: Int
    var id: String
    var isFamily: Bool
    var isFriend: Bool
    var isPublic: Bool
    var owner: String
    var secret: String
    var server: String
    var title: String
    
    var thumbURL: String
    var largeURL: String
    
    var has_geo_coords: Bool
    var coords: CLLocationCoordinate2D
        
    
    mutating func setThumbURL() {
        
        self.thumbURL = "https://farm\(self.farm).staticflickr.com/\(self.server)/\(self.id)_\(self.secret)_m.jpg"

    }
    
    mutating func setLargeURL() {
        
        self.largeURL = "https://farm\(self.farm).staticflickr.com/\(self.server)/\(self.id)_\(self.secret)_b.jpg"
        
    }
    
//    let largePhotoURLs = flickrPhotos.map {
//        "https://farm\($0.farm).staticflickr.com/\($0.server)/\($0.id)_\($0.secret)_b.jpg"
//        
//    }
    
    
//    print(largePhotoURLs)

    
    
}
