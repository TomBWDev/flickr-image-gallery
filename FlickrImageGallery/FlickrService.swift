//
//  FlickrService.swift
//  FlickrImageGallery
//
//  Created by Apple on 24/03/2017.
//  Copyright © 2017 Tom. All rights reserved.
//

import Foundation
import CoreLocation

class FlickrService {
    
    
    static let flickrService = FlickrService()
    
    var API_KEY = ""
// "ace70ad638f437514b55595ba82b7126"

//    let NB_IMG_PER_PAGE = 300
//    let PAGE_NB = 1
    
    private init() {
        
        if let config = Bundle.main.path(forResource: "config", ofType: "plist"),
            let configData = NSDictionary(contentsOfFile: config) as? [String: Any] {
            
            // TO DO Include in guard & throw an exception
            self.API_KEY = configData["api_key"] as! String
            
        }

    }

    var session = URLSession.shared

    
    func getThumbImages(with criteria: [String: Any], completionHandler: @escaping ([Photo]?) -> Void) {
        
        
        // Ex. objet JSON image Flickr
        //    { "id": "33587061971", "owner": "130674893@N07", "secret": "3bc95b402e", "server": "2817", "farm": 3, "title": "From the air, South Island NZ", "ispublic": 1, "isfriend": 0, "isfamily": 0 }
        
        //        let thumbsURL = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=6b4b5a5e6c77e4aec3b87e388e63a8e4&tags=airplane&safe_search=&per_page=30&page=1&format=json&nojsoncallback=1")!
        
        guard let nb_imgs_per_page = criteria["NB_IMGS_PER_PAGE"] else {
            print("Criteria NB_IMGS_PER_PAGE is missing")
            return
        }
        
        guard let page = criteria["PAGE"] else {
            print("Criteria PAGE is missing")
            return
        }
        
        struct PhotoIdCoords {
            
            var photoId: String
            var latit: Double
            var longit: Double
            
        }
        
        // Temp URL change for GeoLoc pics
        /*
        let thumbsURL = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(API_KEY)&tags=airplane&safe_search=1&per_page=\(nb_imgs_per_page)&page=\(page)&format=json&nojsoncallback=1")! */
        
        let thumbsURL = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(API_KEY)&tags=animal&has_geo=1&per_page=\(nb_imgs_per_page)&page=\(page)&format=json&nojsoncallback=1")!
        
        // GET
        // Closure will bring our response after GET (default request type)
        let taskThumbs = session.dataTask(with: thumbsURL) { (data, response, error) in
            
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let receivedData = data else {
                print("Missing data when trying to get photos from Flickr")
                return
            }
            
            guard let JSONthumbs = try? JSONSerialization.jsonObject(with: receivedData, options: []) as! [String: Any] else {
                print("JSON serialization error with data from Flickr photo.search API")
                return
            }
            
            guard let photosPack = JSONthumbs["photos"] as? [String : Any] else {
                print("No photosPackage")
                return
            }
            
            guard let photos = photosPack["photo"] as? [[String : Any]] else {
                print("No photo properties")
                return
            }
            
            
            // Loop on photos & for each use id to send a request to geoLocation
            
//            var photoIdCoordsArray: [PhotoIdCoords] = []

            var photoIdCoordsArray = [PhotoIdCoords]()
            
            for photoElement in photos {
                
                let photoId = photoElement["id"] as! String
                
                // Get returned data

                let data2 = self.getImageCoords(photoId)
                                    
                guard let receivedData2 = data2 else {
                    print("Missing data when trying to get photo coords data from Flickr")
                    continue
                }
                
                guard let JSONphotoLocs = try? JSONSerialization.jsonObject(with: receivedData2, options: []) as! [String: Any] else {
                    print("JSON serialization error with data from Flickr getLocation API")
                    continue
                }
                
//                print(JSONphotoLocs)
                
                guard let photoCoords = JSONphotoLocs["photo"] as? [String : Any],
                    JSONphotoLocs["stat"] as? String == "ok"  else {
                    print("No photo part found in JSON data or status not OK")
                    continue
                }
                
//                print(photoCoords)
                
                guard let location = photoCoords["location"] as?  [String : Any] else {
                    print("No photo location property")
                    continue
                }
                
                
                guard let longit = location["longitude"] as? String else {
                    print("No photo longit properties")
                    continue
                }
                
                guard let latit = location["latitude"] as?  String else {
                    print("No photo latit properties")
                    continue
                }
                
                let photoIdCoords = PhotoIdCoords(photoId: photoId,
                                                  latit: Double(latit)!,
                                                  longit: Double(longit)!)
                
                photoIdCoordsArray.append(photoIdCoords)
                // TO DO Store id & coords in a collection of id, latit, longit
                // latit & longit must be cast as Double
            
            }
            
            // Now unserialize JSON data (photos) & put it in an array of Photo objects
            
            let flickrPhotos = photos.map { (photoElement: [String : Any]) -> Photo in
                
                let photoCoords = CLLocationCoordinate2D()
                var photo = Photo(
                    farm: photoElement["farm"] as! Int,
                    id: photoElement["id"] as! String,
                    isFamily: photoElement["isfamily"] as! Bool,
                    isFriend: photoElement["isfriend"] as! Bool,
                    isPublic: photoElement["ispublic"] as! Bool,
                    owner: photoElement["owner"] as! String,
                    secret: photoElement["secret"] as! String,
                    server: photoElement["server"] as! String,
                    title: photoElement["title"] as! String,
                    thumbURL: "",
                    largeURL: "",
                    has_geo_coords: false,
                    coords: photoCoords
                    )
                
                // TO DO Move these methods & properties (to have them available to entity FavPhotos)
                photo.setThumbURL()
                photo.setLargeURL()
                
                // TO DO Search for photoIdCoords for current photo
                // Get coords from photo with same Id in photosCoords
                if (photoIdCoordsArray.first(where: { $0.photoId == photo.id })) == nil {
                    print("No location info for photoId: \(photo.id)")
                    return photo
                }
                
                let latit = photoIdCoordsArray.first(where: { $0.photoId == photo.id })?.latit
                let longit = photoIdCoordsArray.first(where: { $0.photoId == photo.id })?.longit
                
                // TO DO Add method to set coords via
                photo.coords = CLLocationCoordinate2D(latitude: latit! as CLLocationDegrees,
                                                      longitude: longit! as CLLocationDegrees)
                photo.has_geo_coords = true
                
                return photo
                
            }
            
            print(flickrPhotos)
            
            // Return photos array to completionHandler
            completionHandler(flickrPhotos)
            
        }
        
        taskThumbs.resume()
    
    }
    
    func getImageCoords(_ photoId: String) -> Data? {
        
        // TO DO Make this better (?)
        let coordsURL = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.geo.getLocation&api_key=\(self.API_KEY)&photo_id=\(photoId)&format=json&nojsoncallback=1")!
        
        guard let data = try? Data(contentsOf: coordsURL) else {
            print("No coords data found with URL: \(coordsURL)")
            return nil
        }
        
        return data
    }
    
    func getImage(_ photoURL: String) -> Data? {
        
        // TO DO Make this 
        let url = URL(string: photoURL)
        guard let data = try? Data(contentsOf: url!) else {
            print("No photo found with URL: \(photoURL)")
            return nil
        }

        return data
//        let session = URLSession.shared
//        
//        let taskImage = session.dataTask(with: photoURL) { (data, response, error) in
//        
//            guard error == nil else {
//                print(error!)
//                return
//            }
//            
//            guard let receivedData = data else {
//                print("Missing data when trying to get photo from Flickr")
//                return
//            }
        
//             Now get image from receivedData
            
//            guard let JSONthumbs = try? JSONSerialization.jsonObject(with: receivedData, options: []) as! [String: Any] else {
//                print("JSON serialization error with data from Flickr")
//                return
//            }
//            
//            print(JSONthumbs)
            
//        }
        
        
//        taskImage.resume()
    
     
    }
    
    
}
