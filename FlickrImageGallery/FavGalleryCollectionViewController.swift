//
//  FavGalleryCollectionViewController.swift
//  FlickrImageGallery
//
//  Created by Apple on 05/04/2017.
//  Copyright © 2017 Tom. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "fav-image-cell"

class FavGalleryCollectionViewController: UICollectionViewController {

    var favPhotos: [FavPhoto] = []
    var images: [UIImage] = []
    
    let flickrService = FlickrService.flickrService

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Prepare context for Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        /*
        // Create entities
        var favPhoto = NSEntityDescription.insertNewObject(forEntityName: "FavPhoto", into: context) as! FavPhoto
        
        employee.firstname = "Thomas"
        employee.lastname = "Gros"
        
        // Save entities
        appDelegate.saveContext()
        */
        
        context.perform {
            
            // Fetch entities
            let favPhotosFetchRequest: NSFetchRequest<FavPhoto> = FavPhoto.fetchRequest()
            // Set filter criteria (before sending the request)
//            favPhotosFetchRequest.predicate = NSPredicate(format: "firstname == %@", "Thomas")
//            favPhotosFetchRequest.sortDescriptors = [NSSortDescriptor(key: "lastname", ascending: false)]
            
            do {
                let fetchedFavPhotos = try favPhotosFetchRequest.execute()
                print(fetchedFavPhotos)
                self.favPhotos = fetchedFavPhotos
                
                if self.favPhotos.count > 0 {
                    for photo in self.favPhotos {
                        // TO DO Change this by building the URL using photo propperties
                        let photoURL = "la bonne flickr url en string qui va bien"
                        guard let data = self.flickrService.getImage(photoURL) else {
                            print("No image found with requested URL")
                            return
                        }
                        
                        let image = UIImage(data: data)
                        self.images.append(image!)
                        
                        
                    }
                    
                    // Load cells visible on 1 page
                    /*
                    DispatchQueue.main.async {
                        self.collectionView?.reloadItems(at: self.cellIndexPaths)
                    }
                    */
                    
                    /* To reload all cells
                     DispatchQueue.main.async {
                     self.collectionView?.reloadData()
                     }
                     */
                    
                } else {
                    print("thumbPhotos empty: No photo object returned via main URL")
                    return
                }
                
            } catch {
                fatalError("Failed to fetch FavPhotos: \(error)")
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
