//
//  GalleryCollectionViewController.swift
//  FlickrImageGallery
//
//  Created by Apple on 23/03/2017.
//  Copyright © 2017 Tom. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "image-cell"

class GalleryCollectionViewController: UICollectionViewController {

    
    var images: [UIImage] = []
    var thumbPhotos: [Photo] = []
    var largePhotos: [Photo] = []
    
    let NB_IMGS_PER_PAGE = 15
    let NB_PAGES = 1
    var nbImages: Int = -1
    var criteria: [String: Any] = [:]
    var cellIndexPaths: [IndexPath] = []
    var firstIndexOnPage: Int = -1
    var lastIndexOnPage: Int = -1
    
    let flickrService = FlickrService.flickrService
    
    @IBAction func doubleTap(_ sender: Any) {
        
         
        
        saveImageToFS(from: sender as! UIImageView)
        
        saveImageAsFav(from: sender as! UIImageView)

    }
    
    func saveImageToFS(from sender: UIImageView) -> Void {
        // Save image to FS (document/images folder)
//        let imageName = "my-image.jpg"
        // TO DO Get photo info which will be used to identify the saved image
        let imageId = "#01"
        let imageName = "my-image\(imageId).jpg"

        if let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            var imagePath = documentDirectoryURL.appendingPathComponent("images", isDirectory: true)
            imagePath = imagePath.appendingPathComponent(imageName)
            
//            if let data = try? Data(contentsOf: imagePath),
//                let image = UIImage(data: data) {

            if let image = sender.image {
                
                do {
                    try UIImageJPEGRepresentation(image, 1.0)?.write(to: imagePath)
                } catch {
                    fatalError("Can't write image \(error)")
                }
                
                // Retrieve saved image from FS
                //                guard let loadedImage = UIImage(contentsOfFile: imagePath.path) else {
                //                    fatalError("Can't read image")
                //                }
                
                // Load
                //                self.imageView.image = loadedImage
                
            }
            
        }

    }
    
    func saveImageAsFav(from sender: UIImageView) -> Void {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        nbImages = NB_IMGS_PER_PAGE * NB_PAGES
        
        criteria["NB_IMGS_PER_PAGE"] = NB_IMGS_PER_PAGE
        
        
        for i in 0..<NB_PAGES {
            
            let page = i + 1
            criteria["PAGE"] = page
            
            firstIndexOnPage = (page * NB_IMGS_PER_PAGE) - NB_IMGS_PER_PAGE
            lastIndexOnPage = firstIndexOnPage + NB_IMGS_PER_PAGE - 1
            
            cellIndexPaths = []
            for j in firstIndexOnPage...lastIndexOnPage {
                // Only & first section has index 0
                let myIndexPath = IndexPath(item: j, section: 0)
                
                cellIndexPaths.append(myIndexPath)
            }
            
            
            // Get thumb image Photo objects from FlickrService
            flickrService.getThumbImages(with: criteria, completionHandler: {
                
                // Get flickrPhotos from service
                self.thumbPhotos = $0!
                
                if self.thumbPhotos.count > 0 {
                    for photo in self.thumbPhotos {
                        guard let data = self.flickrService.getImage(photo.thumbURL) else {
                            print("No image found with requested URL")
                            return
                        }
                        
                        let image = UIImage(data: data)
                        self.images.append(image!)
                        
                        
                    }
                    
                    // Load cells visible on 1 page
                    
                    DispatchQueue.main.async {
                        self.collectionView?.reloadItems(at: self.cellIndexPaths)
                    }
                    
                    
                    /* To reload all cells
                     DispatchQueue.main.async {
                     self.collectionView?.reloadData()
                     }
                     */
                    
                } else {
                    print("thumbPhotos empty: No photo object returned via main URL")
                    return
                }
                
            } )
            

        }

                // Get large images Photo objects from FlickrService
        
        /*
         Map thumb images URL strings from thumbPhotos
         Image URL format: "https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)"
        for photo in thumbPhotos {
            thumbURLs.append(photo.getThumbURL())
        }
        
         Map large images URL strings from largePhotos
        
        if let path = Bundle.main.path(forResource: "imageUrls", ofType: "plist"),
            let imageUrlsData = NSArray(contentsOfFile: path) as? [String] {
        
            
            images = imageUrlsData.map {
                
                let decodedUrl = $0.removingPercentEncoding
                let url = URL(string: decodedUrl!)
                
                let data = try? Data(contentsOf: url!)
                
                let image = UIImage(data: data!)
                return image!
            }
        
        }
         */
        // Do any additional setup after loading the view.
    } // end of override func viewDidLoad()

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
//        return images.count
        return nbImages
//        return 30
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
                as! ImageCollectionViewCell
    
        // Configure the cell
        
//        guard let data = flickrService.getImage(thumbPhotos[indexPath.row].thumbURL) else {
//            print("No image found with requested URL")
//            return cell
//        }
//        
//        let image = UIImage(data: data)
        
        if images.count == 0 {
            
            print("images empty: No image available")
            return cell
            
        }
        
        cell.imageView.image = images[indexPath.item]
        
        print(indexPath.item)
//        cell.becomeFirstResponder()
//        cell.reloadInputViews()
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "image-detail-segue" {
            
            guard let currentCell = sender as? ImageCollectionViewCell else {
                print("No cell selected while preparing segue")
                return // Do nothing
            }
            
            guard let selectedIndexPath = collectionView?.indexPath(for: currentCell) else {
                print("No row selected while preparing segue")
                return // Do nothing
            }
            
            guard let imageDetailViewController = segue.destination as? ImageDetailViewController else {
                print("imageDetail segue destination controller not of type ImageDetailViewController")
                return // Do nothing
            }
            
            imageDetailViewController.image = images[selectedIndexPath.row]
            
        }
        
    }

    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
